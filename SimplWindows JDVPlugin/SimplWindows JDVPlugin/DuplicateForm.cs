﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimplWindows_JDVPlugin
{
    public partial class DuplicateForm : Form
    {
        private MainForm parent;
        private bool editMode = false;
        private int editIndex;

        public DuplicateForm(MainForm form)
        {
            InitializeComponent();
            parent = form;
            editMode = false;
            this.Text = "Add new list to duplicate";
        }

        public DuplicateForm(MainForm form, string name, string replace, string[] data, int editIndex)
        {
            InitializeComponent();
            parent = form;
            txtName.Text = name;
            txtReplace.Text = replace;
            txtList.Text = String.Join("\r\n", data);
            this.editIndex = editIndex;
            editMode = true;
            this.Text = "Edit list to duplicate";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var name = txtName.Text;
            var replace = txtReplace.Text;
            var list = txtList.Text.Replace("\r","").Split(new string[] {"\n"}, StringSplitOptions.RemoveEmptyEntries);

            if (!name.Equals("") && list.Length >= 1 && !list[0].Equals(""))
            {
                if (editMode)
                {
                    parent.EditListToDup(editIndex, name, replace, list);
                }
                else
                {
                    parent.AddListToDup(name, replace, list);
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("You have to enter name and at least one item in the list.");
            }
        }

    }
}
