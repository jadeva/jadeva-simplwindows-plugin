﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;


namespace SimplWindows_JDVPlugin
{
    public partial class MainForm : Form
    {
        const int cMaxLastImports = 5;

        const string cPatternComment = @"\b(Cmn\d+={1}.*?)<search>{1}(.*?\x0A{1})";
        const string cPatternInput = @"\b(I\d+={1}.*?)<search>{1}(.*?\x0A{1})";
        const string cPatternOutput = @"\b(O\d+={1}.*?)<search>{1}(.*?\x0A{1})";
        const string cPatternParameter = @"\b(P\d+={1}.*?)<search>{1}(.*?\x0A{1})";
        const string cPatternSignalMulti = @"(.*?)<search>{1}(.*?,\d+,\d+,\d+\x7F{1})";
        const string cPatternParameterMulti = @"(.*?)<search>{1}(.*?\x7F{1})";
        

        string cDataBaseFolder = "Nm=\nPrH=4\nParentDspSymH=0\nSmC=156\nn1I=0\nn2I=0\nn1O=0\nOpF=1\nCF=2\nSysF=0\nCF=2\nLc=0\nCmn1=<folder_name>\\\\\nC1=(null)\nmI=0\nmO=0\ntO=0\nmP=0\n\x7F";
        string cDataLastItem = " Nm=\nPrH=260\nSmC=121\nn1I=0\nn2I=0\nn1O=0\nOpF=1\nCF=2\nSysF=0\nCF=2\nLc=0\nmI=0\nmO=0\ntO=0\nmP=1\nP1=\"\"\n";

        private string clipboardData;

        private DuplicateForm dupForm;
        private int dupFormEditIndex = -1;
        private List<DupListItem> dupListItems = new List<DupListItem>();
        private List<string> lastImports;

        Timer animTimer = new Timer();
        int initHeight;

        private enum ReplaceType
        {
            Increment,
            List
        }

        public MainForm()
        {
            InitializeComponent();

            //Load list from settings.
            try
            {
                lastImports = JsonConvert.DeserializeObject<List<string>>(Properties.Settings.Default.LastImports);
                dupListItems = JsonConvert.DeserializeObject<List<DupListItem>>(Properties.Settings.Default.DupListItems);
            }
            catch
            {
                Console.WriteLine("No data found to load from settings");
            }

            if (lastImports == null)
            {
                lastImports = new List<string>();
            }
            if (dupListItems == null)
            {
                dupListItems = new List<DupListItem>();
            }
            UpdateDupList();

            initHeight = this.Height;
            Timer adTimer = new Timer();
            adTimer.Interval = 10000;
            adTimer.Tick += adTimer_Tick;
            adTimer.Start();
        }

        void adTimer_Tick(object sender, EventArgs e)
        {
            Timer timer = (Timer)sender;
            timer.Stop();
            timer.Dispose();
            HideBottomBarAnimated();
        }

            

        void HideBottomBarAnimated()
        {
            animTimer.Interval = 15;
            animTimer.Tick += animTimer_Tick;
            animTimer.Start();
        }

        void animTimer_Tick(object sender, EventArgs e)
        {
            if (this.Height > initHeight - 40)
            {
                this.Height -= 1;
            }
            else
            {
                animTimer.Stop();
                animTimer.Dispose();
            }   
        }


        private string ReplaceAll(string data, string replace, string replacement, string rawPattern, bool ignoreCase)
        {
            string pattern = rawPattern.Replace("<search>", replace);
            string replacementPattern = "${1}" + replacement + "${2}";

            string output = "";
            if (ignoreCase)
            {
                output = Regex.Replace(data, pattern, replacementPattern, RegexOptions.IgnoreCase);
            }
            else 
            {
                output = Regex.Replace(data, pattern, replacementPattern);
            }

            return output.TrimEnd('\0');
        }


        private string MergeSymbolData(string[] data, string folderName)
        {
            
            string allData = cDataBaseFolder.Replace("<folder_name>", folderName);

            foreach (string d in data)
            {
                allData += d;
                allData += "\x7F";
            }
            allData += cDataLastItem;

            return allData;
        }





        private void Replace(ReplaceType type, string[] list)
        {
            var replacement = "";
            var replace = "";
            var copies = 0;
            var offset = 0;
            var padding = 0;

            ClipboardMethods.DataType dataType;
            clipboardData = ClipboardMethods.FetchClipboardData(out dataType);

            if (dataType == ClipboardMethods.DataType.None)
            {
                MessageBox.Show("No data from SimplWindows found in clipboard. Please use \"Ctrl + C\" in SimplWindows to supply data.");
                return;
            }

            if (type == ReplaceType.Increment)
            {
                if (numIncrementCopies.Value < 1)
                {
                    MessageBox.Show("You have to make at least one copy.");
                    return;
                }

                replacement = txtIncrementReplace.Text;

                if (txtIncrementReplace.Text.Equals("") || replacement.IndexOf("##") == 0)
                {
                    MessageBox.Show("You have to supply a word with ## showing what to increment.\nExample: Output_##");
                    return;
                }

                replace = replacement.Replace("##", @"(?:\d+)");
                copies = (int)numIncrementCopies.Value;
                offset = (int)numIncrementStart.Value;
                padding = (int)numPadding.Value;
            }
            else if (type == ReplaceType.List)
            {
                replace = txtDupReplace.Text;
                copies = list.Length;

                var replaces = replace.Split(';');
                foreach (var l in list)
                {
                    
                    if (l.Split(';').Length != replaces.Length)
                    {
                        MessageBox.Show("The ';' is used for replacing multiple strings. All entries in the list must have the same amount of replaces as the base string.");
                        return;
                    }    
                }

                
            }

            var cb = clipboardData;

            switch (dataType)
            {
                case ClipboardMethods.DataType.ProgramSymbol :
                    ReplaceProgramSymbol(type, copies, clipboardData, replace, replacement, offset, list, padding);
                    break;

                case ClipboardMethods.DataType.SignalMulti :
                    ReplaceSignal(type, copies, clipboardData, replace, replacement, offset, list, padding);
                    break;

                case ClipboardMethods.DataType.Signal:
                    var sigdata = clipboardData.Split(',')[1];
                    sigdata = sigdata.TrimEnd('\0');
                    sigdata += ",0,0,0\x7F";
                    ReplaceSignal(type, copies, sigdata, replace, replacement, offset, list, padding);
                    break;

                case ClipboardMethods.DataType.ParameterMulti:
                    ReplaceParameter(type, copies, clipboardData, replace, replacement, offset, list, padding);
                    break;

                case ClipboardMethods.DataType.Parameter:
                    var paramdata = clipboardData.Split(',')[1];
                    paramdata = paramdata.TrimEnd('\0');
                    paramdata += "\x7F";
                    ReplaceParameter(type, copies, paramdata, replace, replacement, offset, list, padding);
                    break;
            }

        }

        private void ReplaceProgramSymbol(ReplaceType type, int copies, string cb, string replace, string replacement, int offset, string[] list, int padding)
        {
            var dataHolder = new string[copies];

            for (int i = 0; i < copies; i++)
            {
                string data = cb;
                string[] replacementStrings;
                string[] replaces = replace.Split(';');

                if (type == ReplaceType.Increment)
                {
                    replacementStrings = new string[] { replacement.Replace("##", offset.ToString("D" + padding.ToString())) };
                }
                else
                {
                    
                    replacementStrings = list[i].Split(';');
                }
                for (var j = 0; j < replacementStrings.Length; j++)
                {

                    if (cboxInputs.Checked)
                    {
                        data = ReplaceAll(data, replaces[j], replacementStrings[j], cPatternInput, cboxIgnoreCase.Checked);
                    }
                    if (cboxOutputs.Checked)
                    {
                        data = ReplaceAll(data, replaces[j], replacementStrings[j], cPatternOutput, cboxIgnoreCase.Checked);
                    }
                    if (cboxParameters.Checked)
                    {
                        data = ReplaceAll(data, replaces[j], replacementStrings[j], cPatternParameter, cboxIgnoreCase.Checked);
                    }
                    if (cboxComments.Checked)
                    {
                        data = ReplaceAll(data, replaces[j], replacementStrings[j], cPatternComment, cboxIgnoreCase.Checked);
                    }
                }
                if (data.IndexOf('\x7F') >= 0)
                {
                    data = "|" + data;
                }

                dataHolder[i] = data;
                offset++;
            }

            var folderName = "- copies -";
            if (type == ReplaceType.List)
            {
                folderName = "- " + replace.Split(';')[0] + " copies -";
            }
            ClipboardMethods.SetClipboardData(ClipboardMethods.DataType.ProgramSymbol, MergeSymbolData(dataHolder, folderName));
            
        }

        private void ReplaceSignal(ReplaceType type, int copies, string cb, string replace, string replacement, int offset, string[] list, int padding)
        {
            var dataHolder = new string[copies];

            for (int i = 0; i < copies; i++)
            {
                string data = cb;
                string[] replacementStrings;
                string[] replaces = replace.Split(';');

                if (type == ReplaceType.Increment)
                {
                    replacementStrings = new string[] { replacement.Replace("##", offset.ToString("D" + padding.ToString())) };
                }
                else
                {
                    replacementStrings = list[i].Split(';');
                }
                for (var j = 0; j < replacementStrings.Length; j++)
                {
                    data = ReplaceAll(data, replaces[j], replacementStrings[j], cPatternSignalMulti, cboxIgnoreCase.Checked);
                }
                dataHolder[i] = data;
                offset++;
            }
            ClipboardMethods.SetClipboardData(ClipboardMethods.DataType.SignalMulti, String.Join("", dataHolder) + "\0");
        }

        private void ReplaceParameter(ReplaceType type, int copies, string cb, string replace, string replacement, int offset, string[] list, int padding)
        {
            var dataHolder = new string[copies];

            for (int i = 0; i < copies; i++)
            {
                string data = cb;
                string[] replacementStrings;
                string[] replaces = replace.Split(';');

                if (type == ReplaceType.Increment)
                {
                    replacementStrings = replacementStrings = new string[] { replacement.Replace("##", offset.ToString("D" + padding.ToString())) };
                }
                else
                {
                    replacementStrings = list[i].Split(';');
                }
                for (var j = 0; j < replacementStrings.Length; j++)
                {
                    data = ReplaceAll(data, replaces[j], replacementStrings[j], cPatternParameterMulti, cboxIgnoreCase.Checked);
                }

                dataHolder[i] = data;
                offset++;
            }
            ClipboardMethods.SetClipboardData(ClipboardMethods.DataType.ParameterMulti, String.Join("", dataHolder) + "\0");
        }

        public void AddListToDup(string name, string replace, string[] list)
        {
            DupListItem newItem = new DupListItem(name, replace, list);
            dupListItems.Add(newItem);
            UpdateDupList();
            listDup.SelectedIndex = dupListItems.IndexOf(newItem);
        }

        public void EditListToDup(int index, string name, string replace, string[] list)
        {
            dupListItems[index].Name = name;
            dupListItems[index].ToReplace = replace;
            dupListItems[index].Data = list;
            UpdateDupList();
        }

        private void UpdateDupList()
        {
            listDup.Items.Clear();
            dupListItems = dupListItems.OrderBy(x => x.Name).ToList(); //Sorts the list based on Name
            foreach (DupListItem d in dupListItems)
            {
                var title = d.Name + " - [" + d.Data.Length + " copies]";
                listDup.Items.Add(title);
            }
        }

        private void Replace_Click()
        {
            if (rbtnIncrement.Checked)
            {
                Replace(ReplaceType.Increment, null);
            }
            else if (rbtnDupList.Checked)
            {
                if (listDup.SelectedIndex >= 0)
                {
                    Replace(ReplaceType.List, dupListItems[listDup.SelectedIndex].Data);
                }
                else
                {
                    MessageBox.Show("You have to select a list.");
                    return;
                }
            }
            else
            {
                return;
            }
            MessageBox.Show("The data has been saved in Clipboard. Use \"Ctrl + V\" in SimplWindows to paste!");
        }


        //BUTTON HANDLERS
        private void btnReplace_Click(object sender, EventArgs e)
        {
            Replace_Click();
        }

        private void rbtnDupList_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnDupList.Checked)
            {
                groupDuplicate.Enabled = true;
                groupIncrement.Enabled = false;
            }
        }

        private void rbtnIncrement_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnIncrement.Checked)
            {
                groupDuplicate.Enabled = false;
                groupIncrement.Enabled = true;
            }
        }

        private void btnDupAdd_Click(object sender, EventArgs e)
        {
            dupForm = new DuplicateForm(this);
            dupForm.ShowDialog(this);
        }

        private void btnDupDelete_Click(object sender, EventArgs e)
        {
            if (listDup.SelectedIndex >= 0)
            {
                dupListItems.RemoveAt(listDup.SelectedIndex);
                UpdateDupList();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            try
            {
                var json = JsonConvert.SerializeObject(dupListItems);
                Properties.Settings.Default.DupListItems = json;
                json = JsonConvert.SerializeObject(lastImports);
                Properties.Settings.Default.LastImports = json;
                Properties.Settings.Default.Save();
            }
            catch (Exception err)
            {
                Console.WriteLine("Could not save settings: {0}", err.Message);
            }
        }

        private void listDup_Changed(object sender, EventArgs e)
        {
            if (listDup.SelectedIndex >= 0)
            {
                var toReplace = dupListItems[listDup.SelectedIndex].ToReplace;
                if (toReplace != null && !toReplace.Equals(""))
                {
                    txtDupReplace.Text = toReplace;
                }
            }
        }

        private void listDup_DoubleClick(object sender, EventArgs e)
        {
            OpenEditDupForm();
        }

        private void btnDupEdit_Click(object sender, EventArgs e)
        {
            OpenEditDupForm();
        }

        private void OpenEditDupForm()
        {
            if (listDup.SelectedIndex >= 0)
            {
                dupForm = new DuplicateForm(this, 
                                            dupListItems[listDup.SelectedIndex].Name, 
                                            dupListItems[listDup.SelectedIndex].ToReplace,
                                            dupListItems[listDup.SelectedIndex].Data, 
                                            listDup.SelectedIndex);
                dupForm.ShowDialog(this);
            }
            else
            {
                MessageBox.Show("You have to select a item in the list to edit.");
            }
        }

        private void SelectText(object sender, EventArgs e)
        {
            if (sender.GetType() == typeof(TextBox))
            {
                var t = (TextBox)sender;
                t.SelectionStart = 0;
                t.SelectionLength = t.Text.Length;
                return;
            }
            else if (sender.GetType() == typeof(NumericUpDown))
            {
                var n = (NumericUpDown)sender;
                n.Select(0, n.Text.Length);
                return;
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            var menu = new ContextMenu();
            menu.MenuItems.Add("Browse...", onImportClicked);

            if (lastImports.Count > 0)
            {
                menu.MenuItems.Add("-");

                for (int i = 0; i < lastImports.Count; i++)
                {
                    menu.MenuItems.Add(lastImports[i], onImportPathClicked);
                }
            }

            menu.Show(this, new Point(btn.Location.X, btn.Location.Y + btn.Height));   
        }

        private void onImportClicked(object sender, EventArgs e)
        {
            ImportSettingsDialog();
        }

        private void onImportPathClicked(object sender, EventArgs e) {
            MenuItem item = (MenuItem)sender;
            ImportSettings(item.Text);
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            ExportSettings();
        }

        private void ImportSettingsDialog()
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "JaDeVa SimplWindows Plugin files (*.jdv)|*.jdv";
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                ImportSettings(dialog.FileName);
            }
        }

        private void ImportSettings(string filename)
        {
            try
            {
                string json = System.IO.File.ReadAllText(filename);
                dupListItems = JsonConvert.DeserializeObject<List<DupListItem>>(json);
                UpdateDupList();
                txtDupReplace.Text = "";
                InsertLastImport(filename);
            }
            catch (Exception e)
            {
                MessageBox.Show("Ett fel inträffade när filen skulle laddas.\n\n" + e.Message);
            }

            
        }

        private void ExportSettings()
        {
            var dialog = new SaveFileDialog();
            dialog.Filter = "JaDeVa SimplWindows Plugin files (*.jdv)|*.jdv";
            dialog.RestoreDirectory = true;
            dialog.FileName = "CopyPaste.jdv";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var json = JsonConvert.SerializeObject(dupListItems);
                System.IO.File.WriteAllText(dialog.FileName, json);
                InsertLastImport(dialog.FileName);
            }
        }

        private void InsertLastImport(string filename)
        {
            if (lastImports.Contains(filename))
            {
                lastImports.Remove(filename);
                lastImports.Insert(0, filename);
            }
            else
            {
                lastImports.Insert(0, filename);
                if (lastImports.Count > cMaxLastImports)
                {
                    lastImports.RemoveAt(lastImports.Count - 1);
                }
            }
        }

        private void onKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F10)
            {
                Replace_Click();
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.jadeva.se");
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Are you sure you want to start a new \"project\"?", "New project?", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
            {
                txtIncrementReplace.Text = "";
                txtDupReplace.Text = "";
                dupListItems = new List<DupListItem>();
                UpdateDupList();
            }
        }
       

    }

    [Serializable()]
    class DupListItem
    {
        public string Name;
        public string ToReplace;
        public string[] Data;

        public DupListItem(string name, string replace, string[] data)
        {
            Name = name;
            ToReplace = replace;
            Data = data;
        }
    }

}
