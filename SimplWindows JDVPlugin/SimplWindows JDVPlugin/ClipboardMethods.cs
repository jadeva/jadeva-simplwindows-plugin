﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;


namespace SimplWindows_JDVPlugin
{
    static class ClipboardMethods
    {
        public const string cCrestronProgramSymbol = "CM_PROGRAM_SYMBOL";
        public const string cCrestronSignalMulti = "CM_SIGNAL_MULTI";
        public const string cCrestronSignal = "CM_SIGNAL";
        public const string cCrestronParameterMulti = "CM_PARAMETER_MULTI";
        public const string cCrestronParameter = "CM_PARAMETER";

        public enum DataType  {
            None,
            ProgramSymbol,
            SignalMulti,
            Signal,
            ParameterMulti,
            Parameter
        }


        public static string FetchClipboardData(out DataType dataType)
        {
            string dataTypeString = "";
            if (Clipboard.ContainsData(cCrestronProgramSymbol))
            {
                dataType = DataType.ProgramSymbol;
                dataTypeString = cCrestronProgramSymbol;
            }
            else if (Clipboard.ContainsData(cCrestronSignalMulti))
            {
                dataType = DataType.SignalMulti;
                dataTypeString = cCrestronSignalMulti;
            }
            else if (Clipboard.ContainsData(cCrestronSignal))
            {
                dataType = DataType.Signal;
                dataTypeString = cCrestronSignal;
            }
            else if (Clipboard.ContainsData(cCrestronParameterMulti))
            {
                dataType = DataType.ParameterMulti;
                dataTypeString = cCrestronParameterMulti;
            }
            else if (Clipboard.ContainsData(cCrestronParameter))
            {
                dataType = DataType.Parameter;
                dataTypeString = cCrestronParameter;
            }
            else
            {
                dataType = DataType.None;
                return null;
            }
        
            MemoryStream ms = (MemoryStream)Clipboard.GetData(dataTypeString);
            ms.Position = 0;

            StreamReader sr = new StreamReader(ms, Encoding.GetEncoding("latin1"));            
            return sr.ReadToEnd();
        }

        public static void SetClipboardData(DataType dataType, string data)
        {
            string dataTypeString = "";
            switch (dataType)
            {
                case DataType.ProgramSymbol : dataTypeString = cCrestronProgramSymbol; break;
                case DataType.SignalMulti : dataTypeString = cCrestronSignalMulti; break;
                case DataType.Signal : dataTypeString = cCrestronSignal; break;
                case DataType.ParameterMulti : dataTypeString = cCrestronParameterMulti; break;
                case DataType.Parameter : dataTypeString = cCrestronParameter; break;
            }

            Clipboard.Clear();
            MemoryStream ms = new MemoryStream();
            byte[] bytes = Encoding.GetEncoding("latin1").GetBytes(data);
            ms.Write(bytes, 0, bytes.Length);

            Clipboard.SetData(dataTypeString, ms);
        }
    }
}
