﻿namespace SimplWindows_JDVPlugin
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnReplace = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboxComments = new System.Windows.Forms.CheckBox();
            this.cboxParameters = new System.Windows.Forms.CheckBox();
            this.cboxOutputs = new System.Windows.Forms.CheckBox();
            this.cboxInputs = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cboxIgnoreCase = new System.Windows.Forms.CheckBox();
            this.groupIncrement = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.numPadding = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIncrementReplace = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numIncrementStart = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numIncrementCopies = new System.Windows.Forms.NumericUpDown();
            this.rbtnIncrement = new System.Windows.Forms.RadioButton();
            this.rbtnDupList = new System.Windows.Forms.RadioButton();
            this.groupDuplicate = new System.Windows.Forms.GroupBox();
            this.btnDupEdit = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDupReplace = new System.Windows.Forms.TextBox();
            this.btnDupDelete = new System.Windows.Forms.Button();
            this.btnDupAdd = new System.Windows.Forms.Button();
            this.listDup = new System.Windows.Forms.ListBox();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnNew = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupIncrement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPadding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numIncrementStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numIncrementCopies)).BeginInit();
            this.groupDuplicate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnReplace
            // 
            this.btnReplace.Location = new System.Drawing.Point(285, 381);
            this.btnReplace.Name = "btnReplace";
            this.btnReplace.Size = new System.Drawing.Size(107, 33);
            this.btnReplace.TabIndex = 7;
            this.btnReplace.Text = "Replace (F10)";
            this.btnReplace.UseVisualStyleBackColor = true;
            this.btnReplace.Click += new System.EventHandler(this.btnReplace_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboxComments);
            this.groupBox1.Controls.Add(this.cboxParameters);
            this.groupBox1.Controls.Add(this.cboxOutputs);
            this.groupBox1.Controls.Add(this.cboxInputs);
            this.groupBox1.Location = new System.Drawing.Point(285, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(107, 128);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "What to replace";
            // 
            // cboxComments
            // 
            this.cboxComments.AutoSize = true;
            this.cboxComments.Checked = true;
            this.cboxComments.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxComments.Location = new System.Drawing.Point(15, 92);
            this.cboxComments.Name = "cboxComments";
            this.cboxComments.Size = new System.Drawing.Size(75, 17);
            this.cboxComments.TabIndex = 23;
            this.cboxComments.Text = "Comments";
            this.cboxComments.UseVisualStyleBackColor = true;
            // 
            // cboxParameters
            // 
            this.cboxParameters.AutoSize = true;
            this.cboxParameters.Checked = true;
            this.cboxParameters.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxParameters.Location = new System.Drawing.Point(15, 69);
            this.cboxParameters.Name = "cboxParameters";
            this.cboxParameters.Size = new System.Drawing.Size(79, 17);
            this.cboxParameters.TabIndex = 22;
            this.cboxParameters.Text = "Parameters";
            this.cboxParameters.UseVisualStyleBackColor = true;
            // 
            // cboxOutputs
            // 
            this.cboxOutputs.AutoSize = true;
            this.cboxOutputs.Checked = true;
            this.cboxOutputs.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxOutputs.Location = new System.Drawing.Point(15, 46);
            this.cboxOutputs.Name = "cboxOutputs";
            this.cboxOutputs.Size = new System.Drawing.Size(63, 17);
            this.cboxOutputs.TabIndex = 21;
            this.cboxOutputs.Text = "Outputs";
            this.cboxOutputs.UseVisualStyleBackColor = true;
            // 
            // cboxInputs
            // 
            this.cboxInputs.AutoSize = true;
            this.cboxInputs.Checked = true;
            this.cboxInputs.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxInputs.Location = new System.Drawing.Point(15, 23);
            this.cboxInputs.Name = "cboxInputs";
            this.cboxInputs.Size = new System.Drawing.Size(55, 17);
            this.cboxInputs.TabIndex = 20;
            this.cboxInputs.Text = "Inputs";
            this.cboxInputs.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cboxIgnoreCase);
            this.groupBox2.Location = new System.Drawing.Point(285, 151);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(107, 57);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Options";
            // 
            // cboxIgnoreCase
            // 
            this.cboxIgnoreCase.AutoSize = true;
            this.cboxIgnoreCase.Checked = true;
            this.cboxIgnoreCase.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxIgnoreCase.Location = new System.Drawing.Point(15, 24);
            this.cboxIgnoreCase.Name = "cboxIgnoreCase";
            this.cboxIgnoreCase.Size = new System.Drawing.Size(83, 17);
            this.cboxIgnoreCase.TabIndex = 24;
            this.cboxIgnoreCase.Text = "Ignore Case";
            this.cboxIgnoreCase.UseVisualStyleBackColor = true;
            // 
            // groupIncrement
            // 
            this.groupIncrement.Controls.Add(this.label6);
            this.groupIncrement.Controls.Add(this.numPadding);
            this.groupIncrement.Controls.Add(this.label2);
            this.groupIncrement.Controls.Add(this.txtIncrementReplace);
            this.groupIncrement.Controls.Add(this.label3);
            this.groupIncrement.Controls.Add(this.numIncrementStart);
            this.groupIncrement.Controls.Add(this.label1);
            this.groupIncrement.Controls.Add(this.numIncrementCopies);
            this.groupIncrement.Location = new System.Drawing.Point(12, 12);
            this.groupIncrement.Name = "groupIncrement";
            this.groupIncrement.Size = new System.Drawing.Size(255, 162);
            this.groupIncrement.TabIndex = 1;
            this.groupIncrement.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Zero padding (numbers):";
            // 
            // numPadding
            // 
            this.numPadding.Location = new System.Drawing.Point(174, 128);
            this.numPadding.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numPadding.Name = "numPadding";
            this.numPadding.Size = new System.Drawing.Size(65, 20);
            this.numPadding.TabIndex = 21;
            this.numPadding.Enter += new System.EventHandler(this.SelectText);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Replaces ## with a number";
            // 
            // txtIncrementReplace
            // 
            this.txtIncrementReplace.Location = new System.Drawing.Point(17, 49);
            this.txtIncrementReplace.Name = "txtIncrementReplace";
            this.txtIncrementReplace.Size = new System.Drawing.Size(222, 20);
            this.txtIncrementReplace.TabIndex = 2;
            this.txtIncrementReplace.Enter += new System.EventHandler(this.SelectText);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Start on number:";
            // 
            // numIncrementStart
            // 
            this.numIncrementStart.Location = new System.Drawing.Point(174, 76);
            this.numIncrementStart.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numIncrementStart.Name = "numIncrementStart";
            this.numIncrementStart.Size = new System.Drawing.Size(65, 20);
            this.numIncrementStart.TabIndex = 3;
            this.numIncrementStart.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numIncrementStart.Enter += new System.EventHandler(this.SelectText);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Copies:";
            // 
            // numIncrementCopies
            // 
            this.numIncrementCopies.Location = new System.Drawing.Point(174, 102);
            this.numIncrementCopies.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numIncrementCopies.Name = "numIncrementCopies";
            this.numIncrementCopies.Size = new System.Drawing.Size(65, 20);
            this.numIncrementCopies.TabIndex = 4;
            this.numIncrementCopies.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numIncrementCopies.Enter += new System.EventHandler(this.SelectText);
            // 
            // rbtnIncrement
            // 
            this.rbtnIncrement.AutoSize = true;
            this.rbtnIncrement.Checked = true;
            this.rbtnIncrement.Location = new System.Drawing.Point(23, 10);
            this.rbtnIncrement.Name = "rbtnIncrement";
            this.rbtnIncrement.Size = new System.Drawing.Size(132, 17);
            this.rbtnIncrement.TabIndex = 1;
            this.rbtnIncrement.TabStop = true;
            this.rbtnIncrement.Text = "Increase string number";
            this.rbtnIncrement.UseVisualStyleBackColor = true;
            this.rbtnIncrement.CheckedChanged += new System.EventHandler(this.rbtnIncrement_CheckedChanged);
            // 
            // rbtnDupList
            // 
            this.rbtnDupList.AutoSize = true;
            this.rbtnDupList.Location = new System.Drawing.Point(23, 182);
            this.rbtnDupList.Name = "rbtnDupList";
            this.rbtnDupList.Size = new System.Drawing.Size(161, 17);
            this.rbtnDupList.TabIndex = 2;
            this.rbtnDupList.TabStop = true;
            this.rbtnDupList.Text = "Replace and duplicate by list";
            this.rbtnDupList.UseVisualStyleBackColor = true;
            this.rbtnDupList.CheckedChanged += new System.EventHandler(this.rbtnDupList_CheckedChanged);
            // 
            // groupDuplicate
            // 
            this.groupDuplicate.Controls.Add(this.btnDupEdit);
            this.groupDuplicate.Controls.Add(this.label4);
            this.groupDuplicate.Controls.Add(this.txtDupReplace);
            this.groupDuplicate.Controls.Add(this.btnDupDelete);
            this.groupDuplicate.Controls.Add(this.btnDupAdd);
            this.groupDuplicate.Controls.Add(this.listDup);
            this.groupDuplicate.Enabled = false;
            this.groupDuplicate.Location = new System.Drawing.Point(12, 184);
            this.groupDuplicate.Name = "groupDuplicate";
            this.groupDuplicate.Size = new System.Drawing.Size(255, 230);
            this.groupDuplicate.TabIndex = 5;
            this.groupDuplicate.TabStop = false;
            // 
            // btnDupEdit
            // 
            this.btnDupEdit.Location = new System.Drawing.Point(92, 152);
            this.btnDupEdit.Name = "btnDupEdit";
            this.btnDupEdit.Size = new System.Drawing.Size(68, 23);
            this.btnDupEdit.TabIndex = 32;
            this.btnDupEdit.Text = "Edit...";
            this.btnDupEdit.UseVisualStyleBackColor = true;
            this.btnDupEdit.Click += new System.EventHandler(this.btnDupEdit_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Substring to replace:";
            // 
            // txtDupReplace
            // 
            this.txtDupReplace.Location = new System.Drawing.Point(15, 200);
            this.txtDupReplace.Name = "txtDupReplace";
            this.txtDupReplace.Size = new System.Drawing.Size(222, 20);
            this.txtDupReplace.TabIndex = 6;
            this.txtDupReplace.Enter += new System.EventHandler(this.SelectText);
            // 
            // btnDupDelete
            // 
            this.btnDupDelete.Location = new System.Drawing.Point(169, 152);
            this.btnDupDelete.Name = "btnDupDelete";
            this.btnDupDelete.Size = new System.Drawing.Size(68, 23);
            this.btnDupDelete.TabIndex = 31;
            this.btnDupDelete.Text = "Delete";
            this.btnDupDelete.UseVisualStyleBackColor = true;
            this.btnDupDelete.Click += new System.EventHandler(this.btnDupDelete_Click);
            // 
            // btnDupAdd
            // 
            this.btnDupAdd.Location = new System.Drawing.Point(15, 152);
            this.btnDupAdd.Name = "btnDupAdd";
            this.btnDupAdd.Size = new System.Drawing.Size(68, 23);
            this.btnDupAdd.TabIndex = 30;
            this.btnDupAdd.Text = "Add...";
            this.btnDupAdd.UseVisualStyleBackColor = true;
            this.btnDupAdd.Click += new System.EventHandler(this.btnDupAdd_Click);
            // 
            // listDup
            // 
            this.listDup.ColumnWidth = 166;
            this.listDup.FormattingEnabled = true;
            this.listDup.Location = new System.Drawing.Point(16, 25);
            this.listDup.Name = "listDup";
            this.listDup.Size = new System.Drawing.Size(221, 121);
            this.listDup.TabIndex = 5;
            this.listDup.SelectedIndexChanged += new System.EventHandler(this.listDup_Changed);
            this.listDup.DoubleClick += new System.EventHandler(this.listDup_DoubleClick);
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(285, 250);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(107, 23);
            this.btnImport.TabIndex = 11;
            this.btnImport.Text = "Import...";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(285, 279);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(107, 23);
            this.btnExport.TabIndex = 12;
            this.btnExport.Text = "Export...";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(85, 434);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 27);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.label5.Location = new System.Drawing.Point(192, 434);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Developed by JaDeVa AB";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.linkLabel1.Location = new System.Drawing.Point(193, 447);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(80, 13);
            this.linkLabel1.TabIndex = 15;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "www.jadeva.se";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.Location = new System.Drawing.Point(0, 428);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(408, 41);
            this.panel1.TabIndex = 16;
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(285, 221);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(107, 23);
            this.btnNew.TabIndex = 17;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 469);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.rbtnDupList);
            this.Controls.Add(this.groupDuplicate);
            this.Controls.Add(this.rbtnIncrement);
            this.Controls.Add(this.groupIncrement);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnReplace);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "JaDeVa SimplWindows Plugin";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.onKeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupIncrement.ResumeLayout(false);
            this.groupIncrement.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPadding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numIncrementStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numIncrementCopies)).EndInit();
            this.groupDuplicate.ResumeLayout(false);
            this.groupDuplicate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnReplace;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cboxComments;
        private System.Windows.Forms.CheckBox cboxParameters;
        private System.Windows.Forms.CheckBox cboxOutputs;
        private System.Windows.Forms.CheckBox cboxInputs;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cboxIgnoreCase;
        private System.Windows.Forms.GroupBox groupIncrement;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numIncrementStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numIncrementCopies;
        private System.Windows.Forms.TextBox txtIncrementReplace;
        private System.Windows.Forms.RadioButton rbtnIncrement;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtnDupList;
        private System.Windows.Forms.GroupBox groupDuplicate;
        private System.Windows.Forms.Button btnDupAdd;
        private System.Windows.Forms.ListBox listDup;
        private System.Windows.Forms.Button btnDupDelete;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDupReplace;
        private System.Windows.Forms.Button btnDupEdit;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numPadding;
        private System.Windows.Forms.Button btnNew;
    }
}

